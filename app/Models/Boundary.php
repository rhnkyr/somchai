<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @property int $_id
 * @property mixed $name
 * @property mixed $area
 */
class Boundary extends Eloquent
{
    protected $collection = 'boundaries';

}