<?php

namespace App\Http\Controllers;

use App\Models\Boundary;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    /**
     * Calculates cost based on user pickup and drop-off positions
     * @param Request $request
     * @return JsonResponse
     */
    public function quotation(Request $request): JsonResponse
    {
        if (!$request->isJson()) {
            return response()->json(['message' => 'Request does not contain json'], 400);
        }

        if (!$this->checkBoundaries($request)) {
            return response()->json(['message' => 'Can not book in this location '], 200);
        }

        $distance = $this->distance($request);
        $cost     = $this->calculateHelper($distance);

        $data = [
            'from'     => ['lat' => $request->input('from.lat'), 'lng' => $request->input('from.lng')],
            'to'       => ['lat' => $request->input('to.lat'), 'lng' => $request->input('to.lng')],
            'distance' => $distance,
            'cost'     => $cost
        ];

        return response()->json($data);

    }

    /**
     * Check users locations against Somchai's polygon
     * @param Request $request
     * @return bool|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    private function checkBoundaries(Request $request): bool
    {
        //Get lat lon values
        $lat1 = (double)$request->input('from.lat');
        $lng1 = (double)$request->input('from.lng');

        $lat2 = (double)$request->input('to.lat');
        $lng2 = (double)$request->input('to.lng');

        //Query against DB
        $check = Boundary::where('area', 'geoIntersects', [
            '$geometry' => [
                'type'        => 'Point',
                'coordinates' => [$lng1, $lat1]
            ]
        ])->where('area', 'geoIntersects', [
            '$geometry' => [
                'type'        => 'Point',
                'coordinates' => [$lng2, $lat2]
            ]
        ])->count();

        return $check !== 0;
    }

    /**
     * Calculates the cost by distance
     * @param $distance
     * @return float
     */
    private function calculateHelper($distance): float
    {
        return round((float)env('BASE_FEE') + ((float)$distance * (float)env('PER_KM_COST')), 1);
    }

    /**
     * Calculates the distance by location data
     * @param Request $request
     * @return float
     */
    private function distance(Request $request): float
    {
        //todo : here fetch data from google maps api or special api for routing for distance
        //for instance https://maps.googleapis.com/maps/api/distancematrix/json?origins=$request->input('from.lat'),$request->input('from.lng')&destinations=$request->input('to.lat'),$request->input('to.lng')&key=YOUR_API_KEY
        $distance = 1.2;
        return $distance;
    }
}
