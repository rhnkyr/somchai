# Somchai API

created with :

* Lumen - [laravel/lumen](https://github.com/laravel/lumen-framework)
* Laravel-Mongodb - [jenssegers/laravel-mongodb](https://github.com/jenssegers/laravel-mongodb)

Example request

https://documenter.getpostman.com/view/58821/RWTmtHpR

Dummy data example

{ 
    "_id" : ObjectId("5b7181a05025301ca23bd3ed"), 
    "name" : "Somchai", 
    "area" : {
        "type" : "Polygon", 
        "coordinates" : [
            [
                [
                    100.54024, 
                    13.74454
                ], 
                [
                    100.54546, 
                    13.7436
                ], 
                [
                    100.54385, 
                    13.73434
                ], 
                [
                    100.53866, 
                    13.73448
                ], 
                [
                    100.54024, 
                    13.74454
                ]
            ]
        ]
    }
}


